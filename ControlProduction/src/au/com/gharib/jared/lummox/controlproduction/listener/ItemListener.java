package au.com.gharib.jared.lummox.controlproduction.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;

import au.com.gharib.jared.lummox.controlproduction.ControlCore;
import au.com.gharib.jared.lummox.controlproduction.ControlCore.ControlAction;
import au.com.gharib.jared.lummox.controlproduction.Message;
import au.com.gharib.jared.lummox.controlproduction.PlayerData;

public class ItemListener implements Listener {

	@EventHandler
	public void onItemDrop(PlayerDropItemEvent e) {
		Player player   = e.getPlayer();
		PlayerData data = new PlayerData(player);
		ItemStack item  = e.getItemDrop().getItemStack();
		
		if(!data.isInGroup())
			return;
		
		if(!data.canDo(ControlAction.DROP, item.getType(), item.getAmount())) {
			player.sendMessage(ControlCore.getMessage(Message.LIMIT_REACHED));
			e.setCancelled(true);
		} else {
			data.setDone(ControlAction.DROP, item.getType(), 
					data.getDone(ControlAction.DROP, item.getType()) + item.getAmount());
		}
	}
	
	@EventHandler
	public void onItemPickup(PlayerPickupItemEvent e) {
		Player player   = e.getPlayer();
		PlayerData data = new PlayerData(player);
		ItemStack item  = e.getItem().getItemStack();
		
		if(!data.isInGroup())
			return;
		
		if(!data.canDo(ControlAction.PICKUP, item.getType(), item.getAmount())) {
			player.sendMessage(ControlCore.getMessage(Message.LIMIT_REACHED));
			e.setCancelled(true);
		} else {
			data.setDone(ControlAction.PICKUP, item.getType(), 
					data.getDone(ControlAction.PICKUP, item.getType()) + item.getAmount());
		}
	}
	
	@EventHandler
	public void onItemCraft(CraftItemEvent e) {
		Player player   = (Player) e.getWhoClicked();
		PlayerData data = new PlayerData(player);
		ItemStack item  = e.getInventory().getResult();
		
		if(!data.isInGroup())
			return;
		
		if(!data.canDo(ControlAction.CRAFT, item.getType(), item.getAmount())) {
			player.sendMessage(ControlCore.getMessage(Message.LIMIT_REACHED));
			e.setCancelled(true);
		} else {
			data.setDone(ControlAction.CRAFT, item.getType(), 
					data.getDone(ControlAction.CRAFT, item.getType()) + item.getAmount());
		}
	}

	@EventHandler
	public void onFurnaceClick(InventoryClickEvent e) {
		Player player 	 = (Player) e.getWhoClicked();
		PlayerData data  = new PlayerData(player);
		ItemStack toCook = e.getCursor();
		
		if(!data.isInGroup())
			return;
		
		if(e.getInventory().getType() != InventoryType.FURNACE)
			return;
		
		if(e.getSlotType() != SlotType.CRAFTING)
			return;

		if(!data.canDo(ControlAction.SMELT, toCook.getType(), toCook.getAmount())) {
			player.sendMessage(ControlCore.getMessage(Message.LIMIT_REACHED));
			e.setCancelled(true);
		} else {
			data.setDone(ControlAction.SMELT, toCook.getType(), 
					data.getDone(ControlAction.SMELT, toCook.getType()) + toCook.getAmount());
		}
	}

}
